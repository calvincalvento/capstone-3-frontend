

<!doctype html>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link href="/css/animated.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Righteous&display=swap" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Playfair+Display&display=swap" rel="stylesheet"><link href="https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap" rel="stylesheet">
    <link rel="icon" href="/images/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    

    <title>@yield('title')</title>
  </head>
  <body>
  <div id="load"></div>
      
    <div id="page-container">
      <div id="content-wrap">
      <nav class=" navbar navbar-expand-lg navbar-light navbars fixed-top">
        <div class="container">
          <a class="navbar-brand" href="/homepage">  <img src="/images/logo.png" style="height:120px;width:170px;"  alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"> </span>
          </button>
        
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item active">
                <a class="nav-link" href="/homepage">Home<span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link font-weigh-bold" href="/dateList">Date List</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/faqs">FAQS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/contactUs">Contact Us</a>
                  </li>
                  @if(Session::get('user')->status == "admin")
                  <li class="nav-item">
                      <a class="nav-link" href="/userlist">User List</a>
                    </li>
                  @else
                    
                    <li class="nav-item">
                      @if(Session::get('user')->status == "user-verified-confirm" || Session::get('user')->status == "user-verified")
                        <!-- join us trigger modal -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalJoinUs">
    Join Us!
  </button>
  @elseif (Session::get('user')->status == "userDate")
  <form action="/approveUser/{{Session::get('user')->_id}}" method="POST">
    @csrf
    @method('PATCH')
    <input type="text" value="user-verified-confirm" name="approveUser" hidden>
    <button type="submit" class="btn btn-danger w-100">Disable</button>

  </form>
  
 
                        @else
                        <button type="button" class="btn btn-danger text-center w-100" data-toggle="modal" data-target="#exampleModalprofile">
                            Join Us!
                          </button>
                          
                          <!-- Modal -->
                        
                          @endif
                      </li>
                  @endif                  
              <li class="nav-item dropdown">
                <a class="nav-link " href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bars"></i>
                </a>
                
                
                <div class="dropdown-menu text-center" aria-labelledby="navbarDropdown">
                  @if(!empty (Session::get('user')->profilePic))

                  @if(Session::get('user')->status == "user-nonverified")
                  <img  src="/uploads/{{last(Session::get('user')->profilePic)->pictureName}}" style="height:100px;width:100px;border-radius:50%;" >
                  <br>
                  <span class="badge badge-danger">Not Yet Verified</span>
                  @else
                  <img  src="/uploads/{{last(Session::get('user')->profilePic)->pictureName}}" style="height:100px;width:100px;border-radius:50%;" >
                  <br>
                  <span class="badge badge-success">Verified</span>
                  @endif
                  @else
                
                  @if(Session::get('user')->status == "user-nonverified")
                  <img  src="https://www.sccpre.cat/mypng/full/363-3631746_profile-profile-picture-human-face-head-man-woman.png" style="height:100px;width:100px;border-radius:50%;border:2px solid red;" >
                  <br>
                  <span class="badge badge-danger">Not Yet Verified</span>
                  @else
                  <img  src="https://www.sccpre.cat/mypng/full/363-3631746_profile-profile-picture-human-face-head-man-woman.png" style="height:100px;width:100px;border-radius:50%;" >
                  <br>
                  <span class="badge badge-success">Verified</span>
                  @endif
                  @endif
                    <div class="dropdown-divider"></div>
                  @if(Session::get('user'))
                  <p class="dropdown-item text-center" href="#">Hi {{Session::get('user')->name->firstName}}!  </p>
                  <a class="dropdown-item text-center" href="/myProfile">My Profile</a>

                  @if(Session::get('user')->status == "user-nonverified")
                  <a class="dropdown-item text-center bg-danger text-light" href="/verifyAccount">Verify Account</a>
                 @endif
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="/logout">Logout</a>
                  @endif
                  
                </div>
              </li>
             
            </ul>
           
          </div>
        </div>
        </nav>

<div style="margin-top:145px;"></div>
    @yield('content')
      </div>
    <footer id="footer" class="text-center mt-5">
        <h5>follow us on</h5>
        <h1><img src="/images/facebook.png" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/twitter.png" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/gmail.png" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/youtube.png" style="height:50px;width:50px;border-radius:50%;"></h1>
        <p class="text-muted">Copyright CalvinCalvento 2019. All rights reserved
      </footer>
    </div>
    {{-- modal profile --}}
    <div class="modal fade" id="exampleModalprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content ">
            <div class="modal-header ">
               <h4 class="text-dark text-center">You must first need to verify your account to book a date request so we know that your'e a legit user</h4>
      
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        
            <div class="modal-footer">
             
              <a href="/verifyAccount" type="button" class="btn btn-basic">Go To Verify Page</a>
            </div>
          </div>
        </div>
      </div>
       <!-- join us Modal -->
  <div class="modal fade" id="exampleModalJoinUs" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Join us to become a dater and earn!</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>If you join us your profile will be posted on homepage so anyone can see and book you.</p>
            <form action="/joinUs/{{Session::get('user')->_id}}" method="POST">
            <div class="input-group mb-3">
                @csrf
                @method('PATCH')
                <div class="input-group-prepend">
                  <span class="input-group-text">$</span>
                </div>
                <input type="number" class="form-control" aria-label="Amount (to the nearest dollar)" name="pricePerHour" placeholder="Your Price Per Hour" required>
                <div class="input-group-append">
                  <span class="input-group-text">.00</span>
                </div>
                
          </div>
          <div class="input-group mb-3">
              <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2">@example.com</span>
                </div>
              <input type="email" class="form-control" placeholder="Your Paypal Email Address" name="paypalEmail" aria-label="Recipient's username" aria-describedby="basic-addon2" required>
        
            </div>
            <small id="emailHelp" class="form-text text-muted">We will send your commission on your paypal if your date is successful.</small>
          <div class="modal-footer">
          
            <button type="submit" name="userDate" value="userDate" class="btn btn-basic">Join Now!</button>
          </form>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
         document.getElementById('contents').style.visibility="hidden";
    } else if (state == 'complete') {
        setTimeout(function(){
           document.getElementById('interactive');
           document.getElementById('load').style.visibility="hidden";
           document.getElementById('contents').style.visibility="visible";
        },1000);
    }
  }
  </script> 
  
    <script type="text/javascript">
      $(document).ready(function(){
          $('#sfile[type="file"]').change(function(e){
              var fileName = e.target.files[0].name;
              console.log( fileName );
              $('#saveChanges').show()
              $('#Cancel').show();
          });
      });
  </script>
    <script type="text/javascript">
      $(document).ready(function(){
          $('#ssfile[type="file"]').change(function(e){
              var fileName = e.target.files[0].name;
              console.log( fileName );
              $('#saveChanges1').show()
              $('#Cancel1').show();
          });
      });
  </script>
  
  
  </body>
</html>