@extends('layouts.app')

@section('content')
<link href="/css/main.css" rel="stylesheet">

<div class="container">
  
    <div class="row">
        @if(!empty($user->coverPic))
      <div class="col-lg-12 cover-photo d-flex px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2) ), url('/uploads/{{$user->coverPic}}');">
      @else
      <div class="col-lg-12 cover-photo d-flex px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2) ), url('https://images.pexels.com/photos/1417255/pexels-photo-1417255.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=750&w=1260');">
      @endif
      
      
     
      </div>
      </div>
    <div class="row bg-light">
    <div class="col-lg-3 col-md-3 col-sm-12">
       
        <a data-toggle="modal" data-target="#exampleModal" style="cursor:pointer;">
         @if(!empty (($user->profilePic)))
            <img id="output" src="/uploads/{{end($user->profilePic)->pictureName}}" class="w-100  profile-pic">
            @else
            <img id="output" src="https://www.sccpre.cat/mypng/full/363-3631746_profile-profile-picture-human-face-head-man-woman.png" class="w-100  profile-pic">
            @endif
           </a>
           @include('partials.profilepicture')
         
       


         
      </div>
      <div class="col-lg-9 col-md-9 col-sm-12 text-white font-weight-bold" style="margin-top:-50px;">
          <h3 class="username text-capitalize">{{$user->name->firstName}},@if(!empty($user->verification)) {{$user->verification->age}} @endif </h3>
          @if(!empty($user->caption))
          <h4 class="text-dark text-center" style="margin-top:50px;">"{{$user->caption}}"</h4>
          @else
          <h4 class="text-dark text-center font-weight-bold" style="margin-top:50px;">"I like Black black looks good looking good means no dandruff"</h4>
          @endif
        @if(Session::get('user')->status == "user-verified-confirm" || Session::get('user')->status == "user-verified" || Session::get('user')->status == "userDate")
          <form action="/bookRequestUser/{{$user->_id}}" method="POST">
            @csrf
            
            <button type="submit" class="btn btn-danger text-center w-100 font-weight-bold" >BOOK NOW! <i class="fas fa-heart text-white"></i></button>
     </form>
     @else
     <!-- Button trigger modal -->
<button type="button" class="btn btn-danger text-center w-100" data-toggle="modal" data-target="#exampleModalnotverified">
  BOOK NOW!
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalnotverified" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content ">
      <div class="modal-header ">
         <h4 class="text-dark text-center">You must first need to verify your account to book a date request so we know that your'e a legit user</h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
  
      <div class="modal-footer">
       
        <a href="/verifyAccount" type="button" class="btn btn-basic">Go To Verify Page</a>
      </div>
    </div>
  </div>
</div>
     @endif
        
      <div>
    </div>
    </div>
  </div>
  @if(Session::get('editAbout'))
  <div class="alert alert-basic btn-basic alert-dismissible fade show" role="alert">
      <strong>Success!</strong> {{Session::get('editAbout')}}.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <div class="row mt-5">
  
      <div class="col-sm-12 col-lg-4 ">
          <div class="card" style="min-height:400px; ">
              <h5 class="card-header"><strong>My Interest</strong>  </h5>
              
              <div class="card-body">
              <h5>
                @forelse($user->myInterest as  $interest)
               <h4> {{$interest->interestName}}</h4>
                @empty
                no interest yet
                @endforelse
              
              </div>
            </div>
      </div>
      <div class="col-sm-12 col-lg-4">
          <div class="card" style="min-height:400px; ">
              <h5 class="card-header"><strong>About Me</strong> </h5>
          
              <div class="card-body">
                @if(!empty($user->aboutMe))
                  <p>Lives in: <strong style="text-transform:capitalize;">{{$user->verification->address->city}}, {{$user->verification->country}}</strong></p>
              <p>Work as: <strong style="text-transform:capitalize;">{{$user->aboutMe->work}}</strong></p>
                  <p>Height: <strong>{{$user->aboutMe->height}}cm </strong></p>
                  <p>Weight: <strong>{{$user->aboutMe->weight}}kg </strong></p>
                  <p>Relationship: <strong style="text-transform:capitalize;">{{$user->aboutMe->relationship}}</strong> </p>
                  <p>Smoke: <strong style="text-transform:capitalize;">{{$user->aboutMe->smoke}}</strong></p>
                  <p>Drink: <strong style="text-transform:capitalize;">{{$user->aboutMe->drink}}</strong></p>
                  <p>Gender: <strong style="text-transform:capitalize;">{{$user->verification->gender}}</strong></p>
                @else
            
                
                <p>Lives in:</p>
                <p>Work as: <strong style="text-transform:capitalize;"></strong></p>
                  <p>Height: <strong></strong></p>
                  <p>Weight: <strong></strong></p>
                  <p>Relationship: <strong style="text-transform:capitalize;"></strong> </p>
                    <p>Smoke: <strong style="text-transform:capitalize;"></strong></p>
                    <p>Drink: <strong style="text-transform:capitalize;"></strong></p>
                  
                    @endif
              </div>
            </div>
      </div>
      <div class="col-sm-12 col-lg-4">
          <div class="card" style="min-height:400px; ">
              <h5 class="card-header"><strong>I'm Looking for</strong> </h5>
              <div class="card-body">
                @if(!empty ($user->lookingFor))
              <h5>Looking for a <strong>{{$user->lookingFor->gender}}</strong></h5>
<br>
              <h4>"{{$user->lookingFor->description}}"</h4>
              @else
              <h5>Looking for a <strong>Everybody</strong></h5>
              <br>
                            <p>"N/A"</p>
                            @endif
              </div>
            </div>
      </div>
    </div>
    
    <h3 class="mt-3">You might also like</h3>

    <div class="row mt-3 mb-5">
        
                              
                      
    @foreach($usersall as $userall)
    
    <div class="col-lg-4 mt-3 ">
          
          @if(!empty($userall->profilePic))
    <div class="card users px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5) ), url('/uploads/{{end($userall->profilePic)->pictureName}}');">
          @else
          <div class="card users px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5) ), url('https://images.pexels.com/photos/1417255/pexels-photo-1417255.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=750&w=1260');">
                  @endif
                  <div class="card-body " style="position:relative;">
                    <h4 class="text-capitalize card-title p-2 text-white text-center "  style="position:absolute;bottom:50px;left:0;right:0;border-radius:10px; "><strong>{{$userall->name->firstName}}</strong> @if(!empty($userall->verification))
                                  , {{$userall->verification->age}}
                                  @else , N/A
                  @endif
           </h4>
           <form action="/viewProfileUser/{{$userall->_id}}" method="POST">
                  @csrf
                  
                  <button type="submit" class="btn btn-danger text-center w-100" style="position:absolute;bottom:15px;left:0">Date now! <i class="fas fa-heart text-white"></i></button>
           </form>
                  </div>
                </div>
    </div>
    
    @endforeach

  </div>
 


@if(Session::get('verify'))
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin-top:200px;">
      <div class="modal-content">
          <div class="modal-header">
             
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
        <div class="modal-body">
        
          <div class="alert alert-danger" role="alert">
              <p class="text-center">{{Session::get('verify')}}</p>
            </div>
        </div>
      
      </div>
    </div>

      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
          $('#myModal').modal('show');
      });
  </script>
</div>
@endif
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
    };
  </script>
  
@endsection