@extends('layouts.app')
@section('title', 'Date List')

@section('content')



<script
src="https://www.paypal.com/sdk/js?client-id=AbS1IYipTTajMd5VDZh6nnMqeRm58NGQEXhJigRaLB66jfcjjp8Jl9DwMndl__IG6T4XZtsjA4Lm052-">
</script>

<div class="container-fluid mb-5 mt-3" >
  <div class="row">
    <div class="col-lg-4 col-sm-12 mt-4">
      <h3 class="text-center bg-danger py-4 text-white">Pending Date Request</h3>
<div class="mt-3 anyClass">

@forelse($requestsall as $requestall)





@foreach($usersall as $userall)
@if($userall->_id == $requestall->bookerId)
<div class="row mb-5">
    <div class="col-lg-4">
        @if(!empty($userall->profilePic))
        <img src="/uploads/{{end($userall->profilePic)->pictureName}}" style="height:150px;width:100%;">
        @endif
        <form action="/viewProfileConfirm/{{$requestall->_id}}" method="POST">
          @csrf
          @method('PATCH')
          <input type="text" value="{{$requestall->bookerId}}" name="bookerId" hidden>
          <button type="submit" name="pendingConfirm" value="pendingConfirm" class="btn btn-basic w-100 mt-3">View Profile</button>
        </form>
    </div>
    <div class="col-lg-4">
      <h5>Booker Name: <strong class="text-capitalize">{{$userall->name->firstName}}</strong></h5>
      <h5>Age: <strong class="text-capitalize">@if(!empty($userall->verification)) {{$userall->verification->age}} @endif</strong></h5>
      <h5>Location: <strong class="text-capitalize">{{$requestall->location->locationName}}</strong></h5>
    </div>
    <div class="col-lg-4">
        <h5>When: <strong class="text-capitalize">{{\Carbon\Carbon::parse($requestall->date)->isoFormat('MMMM Do YYYY')}}</strong></h5>
        <h5>What Time: <strong class="text-capitalize">{{date('h:i a', strtotime($requestall->time->startTime))}} TO {{date('h:i a', strtotime($requestall->time->endTime))}}</strong></h5>
        <h5>Price: <strong class="text-capitalize">$ {{$requestall->price}}</strong></h5>
        <div class="btn-group mr-2" role="group" aria-label="First group">
             
            <form action="/dateListConfirm/{{$requestall->_id}}" method="POST">
              @csrf
              @method('PATCH')
              <button type="submit" name="pendingConfirm" class="btn btn-basic" value="approveRequest">
                Accept
              </button>
             </form> 
          </div>
          <div class="btn-group" role="group" aria-label="Third group">
              <form action="/dateListConfirm/{{$requestall->_id}}" method="POST">
                @csrf
                @method('PATCH')
                <button type="submit" name="pendingConfirm" class="btn btn-danger" value="declineRequest">
                  Decline
                </button>
               </form> 
            </div>
      
    </div>
  </div>


@endif

@endforeach
@empty

<h3>No Pending Request Found

@endforelse
</div>

    </div>
    <div class="col-lg-4 col-sm-12 mt-4">
   <h3 class="text-center bg-danger py-4 text-white">Who Book Me List</h3>
<div class="container mt-3 anyClass">

@forelse($requestsall1 as $requestall)





@foreach($usersall as $userall)
@if($userall->_id == $requestall->bookerId)
<div class="row mb-5">
    <div class="col-lg-4">
        @if(!empty($userall->profilePic))
        <img src="/uploads/{{end($userall->profilePic)->pictureName}}" style="height:150px;width:100%;">
        @endif
        <form action="/viewProfileUser/{{$userall->_id}}" method="POST">
          @csrf    
          <button type="submit" class="btn btn-basic text-center w-100 mt-3">View Profile!</button>
   </form>
    </div>
    <div class="col-lg-4">
      <h5>Booker Name: <strong class="text-capitalize">{{$userall->name->firstName}}</strong></h5>
      <h5>Age: <strong class="text-capitalize">@if(!empty($userall->verification)) {{$userall->verification->age}} @endif</strong></h5>
      <h5>Location: <strong class="text-capitalize">{{$requestall->location->locationName}}</strong></h5>
   

    </div>
    <div class="col-lg-4">
        <h5>When: <strong class="text-capitalize">{{\Carbon\Carbon::parse($requestall->date)->isoFormat('MMMM Do YYYY')}}</strong></h5>
        <h5>What Time: <strong class="text-capitalize">{{date('h:i a', strtotime($requestall->time->startTime))}} TO {{date('h:i a', strtotime($requestall->time->endTime))}}</strong></h5>
        <h5>Price: <strong class="text-capitalize">$ {{$requestall->price}}</strong></h5>
      
             @if($requestall->status == "approvedRequestConfirm" || $requestall->status == "approveRequest")

             <h5 class="text-success">"You Approved This Request"</h5>
             @elseif ($requestall->status == "paidRequestConfirm")
             <h3 class="text-warning">"Booker Paid This Already"</h3>
             <h5>Contact Number: <strong class="text-capitalize">@if(!empty($userall->verification)) {{$userall->verification->contact}} @endif </strong></h5>
             @elseif  ($requestall->status == "declineRequestConfirm" || $requestall->status == "declineRequest")
             <h3 class="text-danger">"You Decline This Request"</h3>
             @elseif ($requestall->status == "paidRequestDaterConfirm")
             <h5 class="text-success">"Booker Already Paid This"</h5>
             <h5>Contact Number:  <strong class="text-capitalize">@if(!empty($userall->verification)) {{$userall->verification->contact}} @endif </strong></h5>
             @endif
         
      
    </div>
  </div>


@endif

@endforeach
@empty
<h3>No One Book You Yet</h3>
@endforelse


{{-- who i book  --}}
</div>
    </div>
    <div class="col-lg-4 col-sm-12 mt-4">
      <h3 class="text-center bg-danger py-4 text-white">Who I Book List</h3>
<div class="container mt-3 anyClass">

@forelse($requestsall2 as $requestall)





@foreach($usersall as $userall)
@if($userall->_id == $requestall->daterId)
<div class="row mb-5">
    <div class="col-lg-4">
        @if(!empty($userall->profilePic))
        <img src="/uploads/{{end($userall->profilePic)->pictureName}}" style="height:150px;width:100%;">
        @endif
        <form action="/viewProfileUser/{{$userall->_id}}" method="POST">
          @csrf    
          <button type="submit" class="btn btn-basic text-center w-100 mt-3">View Profile!</button>
   </form>
    </div>
    <div class="col-lg-4">
      <h5>Dater Name: <strong class="text-capitalize">{{$userall->name->firstName}}</strong></h5>
      <h5>Age: <strong class="text-capitalize">@if(!empty($userall->verification)) {{$userall->verification->age}} @endif</strong></h5>
      <h5>Location: <strong class="text-capitalize">{{$requestall->location->locationName}}</strong></h5>
         @if (\Carbon\Carbon::parse($requestall->date)->format('mdY') < \Carbon\Carbon::now()->format('mdY'))
      <button type="button" class="btn btn-warning mt-1 w-100">Rate Now!</button>
      @else
      <button type="button" class="btn btn-warning mt-1 w-100" disabled>Rate Now!</button>
      @endif
      
    </div>
    <div class="col-lg-4">
      
        <h5>When: <strong class="text-capitalize">{{\Carbon\Carbon::parse($requestall->date)->isoFormat('MMMM Do YYYY')}}</strong></h5>
        <h5>What Time: <strong class="text-capitalize">{{date('h:i a', strtotime($requestall->time->startTime))}} TO {{date('h:i a', strtotime($requestall->time->endTime))}}</strong></h5>
        <h5>Price: <strong class="text-capitalize">$ {{$requestall->price}}</strong></h5>
      
             @if($requestall->status == "approvedRequestConfirm" || $requestall->status == "approveRequest")
             <h5 class="text-success">"Request Accepted"</h5>
             <h5><strong>Pay Now!</strong></h5>
             <div class="btn-group" role="group" aria-label="Third group">
               <div id="paypal-button-container{{$requestall->price}}"></div>
            
                
                      <script>
                          paypal.Buttons({
                            style: {
                layout: 'horizontal'
            },
                            createOrder: function(data, actions) {
                              // Set up the transaction
                              return actions.order.create({
                                purchase_units: [{
                                  amount: {
                                    value: '{{$requestall->price}}'
                                    
                                  }
                                }]
                              });
                            }
                            
                          }).render('#paypal-button-container{{$requestall->price}}');
                        </script>
                        
                        
                </div> 
             
             @elseif ($requestall->status == "paidRequestConfirm" || $requestall->status == "paidRequestDaterConfirm")
             <h3 class="text-warning">"You Paid This Already"</h3>
             <h5>Contact Number:  <strong class="text-capitalize">@if(!empty($userall->verification)) {{$userall->verification->contact}} @endif </strong></h5>
             @elseif ($requestall->status == "declineRequest" || $requestall->status == "declineRequestConfirm")
             <h3 class="text-danger">"Your Request Is Decline"</h3>
             @else
             <h3 class="text-warning">"Pending"</h3>
             @endif
         
      
    </div>
 
  </div>


@endif

@endforeach
@empty
<h3>You Didn't Book Yet</h3>
@endforelse
    </div>
  </div>
</div>
@endsection