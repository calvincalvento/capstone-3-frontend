@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  </head>

   
<div class="container " style="margin-top:170px;">
 
    @if(Session::get('daterequestSuccess'))
    <div class="alert alert-danger mt-3" role="alert">
        {{Session::get('daterequestSuccess')}}
      </div>
      @endif  
     
    <div class="row mt-3 mb-5">
     
            <div class="col-xl-10 col-md-10 ">
                        <form class="form-inline my-2 my-lg-0" action="/homepage/search" method="POST"> 

                          @csrf
                                        <input class="form-control mr-sm-2 w-100" type="search" placeholder="Search name, age, gender, city, country "name="search"  aria-label="Search">
                                </div>
                                <div class="col-xl-2 col-md-2">
                                        <button class="btn btn-outline-danger my-2 my-sm-0 w-100" type="submit">Search</button>
                                        
                                </form>
                                </div>
        @forelse($usersall as $userall)
        @if($userall->status == 'userDate')
        <div class="col-lg-4 mt-3 ">
                
                @if(!empty($userall->profilePic))
        <div class="card users px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5) ), url('/uploads/{{end($userall->profilePic)->pictureName}}');">
                @else
                <div class="card users px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5) ), url('https://images.pexels.com/photos/1417255/pexels-photo-1417255.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=750&w=1260');">
                        @endif
                        <div class="card-body " style="position:relative;">
                          <h4 class="text-capitalize card-title p-2 text-white text-center"  style="position:absolute;bottom:85px;left:0;right:0;border-radius:10px; "><strong>{{$userall->name->firstName}}</strong> @if(!empty($userall->verification))
                                        , {{$userall->verification->age}}
                                        @else , N/A
                        @endif
                 </h4>
                 <h4 class="text-capitalize card-title p-2 text-white text-center"  style="position:absolute;bottom:50px;left:0;right:0;border-radius:10px; "<strong>@if(!empty($userall->verification)){{$userall->verification->address->province}}, {{$userall->verification->country}}@endif</strong></h4>
                 <form action="/viewProfileUser/{{$userall->_id}}" method="POST">
                        @csrf
                        
                        <button type="submit" class="btn btn-danger text-center w-100" style="position:absolute;bottom:15px;left:0">Date now! <i class="fas fa-heart text-white"></i></button>
                 </form>
                        </div>
                      </div>
        </div>
     @endif
        @endforeach
   
</div>
</div>
@if($user->status == "user-verified")
<div class="modal fade bg-danger" id="myModal" tabindex="-1" role="dialog">
                <div class="modal-dialog " role="document" style="margin-top:200px;">
                  <div class="modal-content bg-basic">
                      <div class="modal-header">
                         <h3 class="text-black">Congratulations!</h3>
                         <form action="/approveUserConfirm/{{$user->_id}}" method="POST">
                                @csrf
                                @method('PATCH')
                                <input type="text" value="user-verified-confirm" name="approveUserConfirm" hidden>
                                <button type="submit" class="close" >
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                      
                              </form>
                       
                      </div>
                    <div class="modal-body">
                    
                      <div class="alert alert-danger" role="alert">
                          <h5 class="text-center">"Your account is now verified!"</h5>
                        </div>
                    </div>
                  
                  </div>
                </div>
              
              
                  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
                <script type="text/javascript">
                  $(window).on('load',function(){
                      $('#myModal').modal('show');
                  });
              </script>
            </div>
            @endif
            {{-- pending request modal --}}
            @foreach($requestsall as $requestall)

            @if($requestall->daterId === Session::get('user')->_id && $requestall->status == "pending")
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" >
    <div class="mx-auto modal-dialog modal-xl" role="document">
      <div class="modal-content" style="height:%;" >
          <div class="modal-header w-100">
             <h3>Congratulations! You have a new date request.</h3>
             <form action="/pendingConfirm/{{$requestall->_id}}" method="POST">
              @csrf
              @method('PATCH')
              <button type="submit" name="pendingConfirm" class="close" value="pendingConfirm">
                <span aria-hidden="true">&times;</span>
              </button>
             </form>
          </div>
        <div class="modal-body">
         @foreach($usersall as $booker)
         @if($booker->_id == $requestall->bookerId)

         <div class="row">
           <div class="col-lg-4 col-sm-3">
             @if(!empty($booker->profilePic))
             <img src="/uploads/{{end($booker->profilePic)->pictureName}}" style="height:200px;width:100%;">
             @endif 
             <h3 class="text-center mt-3">{{$booker->name->firstName}}, 24</h3>
            <form action="/viewProfileConfirm/{{$requestall->_id}}" method="POST">
              @csrf
              @method('PATCH')
              <input type="text" value="{{$requestall->bookerId}}" name="bookerId" hidden>
              <button type="submit" name="pendingConfirm" value="pendingConfirm" class="btn btn-basic w-100 mt-3">View Profile</button>
            </form>

            
           
          </div>
          <div class="col-lg-8 col-sm-9">
              <style>
                  /* Always set the map height explicitly to define the size of the div
                   * element that contains the map. */
                  #map {
                    height: 100%;
                  }
                  /* Optional: Makes the sample page fill the window. */
                  html, body {
                    height: 100%;
                    margin: 0;
                    padding: 0;
                  }
                </style>
               
              <div id="map"></div>
            
              <script>
                function initMap() {
                  var chicago = new google.maps.LatLng({{$requestall->location->latitude}}, {{$requestall->location->longhitude}});
          
                  var map = new google.maps.Map(document.getElementById('map'), {
                    center: chicago,
                    zoom: 19
                  });
          
                  var coordInfoWindow = new google.maps.InfoWindow();
                  coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
                  coordInfoWindow.setPosition(chicago);
                  coordInfoWindow.open(map);
          
                  map.addListener('zoom_changed', function() {
                    coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
                    coordInfoWindow.open(map);
                  });
                }
          
                var TILE_SIZE = 256;
          
                function createInfoWindowContent(latLng, zoom) {
                  var scale = 1 << zoom;
          
                  var worldCoordinate = project(latLng);
          
                  var pixelCoordinate = new google.maps.Point(
                      Math.floor(worldCoordinate.x * scale),
                      Math.floor(worldCoordinate.y * scale));
          
                  var tileCoordinate = new google.maps.Point(
                      Math.floor(worldCoordinate.x * scale / TILE_SIZE),
                      Math.floor(worldCoordinate.y * scale / TILE_SIZE));
          
                  return [
                    "{{$requestall->location->locationName}}",
                
                  ].join('<br>');
                }
          
                // The mapping between latitude, longitude and pixels is defined by the web
                // mercator projection.
                function project(latLng) {
                  var siny = Math.sin(latLng.lat() * Math.PI / 180);
          
                  // Truncating to 0.9999 effectively limits latitude to 89.189. This is
                  // about a third of a tile past the edge of the world tile.
                  siny = Math.min(Math.max(siny, -0.9999), 0.9999);
          
                  return new google.maps.Point(
                      TILE_SIZE * (0.5 + latLng.lng() / 360),
                      TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
                }
              </script>
              <script async defer
              src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVuc0-ACKor2V2jUwh2Hhw3MXDDCOwnTo&callback=initMap">
              </script>
            
          </div>
          <div class="col-lg-4">
              <h5 class="mt-3"><strong>Date: </strong> {{\Carbon\Carbon::parse($requestall->date)->isoFormat('MMMM Do YYYY')}}</h5>
              <h5 class="mt-3"><strong>Date Start Time:</strong> {{date('h:i a', strtotime($requestall->time->startTime))}}</h5>
              <h5 class="mt-3"><strong>Date End Time:</strong> {{date('h:i a', strtotime($requestall->time->endTime))}}</h5>
            </div>
            <div class="col-lg-6">
              <h5 class="mt-3"><strong>Location:</strong> {{$requestall->location->locationName}}
             <h5 class="mt-3"><strong>Price: $ </strong> {{$requestall->price}}</h5>
              <div class="btn-group mr-2" role="group" aria-label="First group">
             
                <form action="/pendingConfirm/{{$requestall->_id}}" method="POST">
                  @csrf
                  @method('PATCH')
                  <button type="submit" name="pendingConfirm" class="btn btn-basic" value="approveRequest">
                    Accept
                  </button>
                 </form> 
              </div>
              <div class="btn-group" role="group" aria-label="Third group">
                  <form action="/pendingConfirm/{{$requestall->_id}}" method="POST">
                    @csrf
                    @method('PATCH')
                    <button type="submit" name="pendingConfirm" class="btn btn-danger" value="declineRequest">
                      Decline
                    </button>
                   </form> 
                </div>  
            </div>
        </div>
       
        
          
      
@endif
         @endforeach
         
        
        </div>
      
      </div>
    </div>
 
  
  
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $(window).on('load',function(){
      $('#myModal1').modal('show');
  });
</script>
@endif
@endforeach
@foreach($requestsall as $requestall)
{{-- approve request modal --}}
@if($requestall->bookerId === Session::get('user')->_id && $requestall->status == "approveRequest")
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" >
<div class="mx-auto modal-dialog modal-xl" role="document">
<div class="modal-content" style="height:%;" >
<div class="modal-header w-100">
 <h3>Congratulations! Your Request Has Been Accepted.</h3>
 <form action="/pendingConfirm/{{$requestall->_id}}" method="POST">
  @csrf
  @method('PATCH')
  <button type="submit" name="pendingConfirm" class="close" value="approvedRequestConfirm">
    <span aria-hidden="true">&times;</span>
  </button>
 </form>
</div>
<div class="modal-body">
@foreach($usersall as $booker)
@if($booker->_id == $requestall->daterId)

<div class="row">
<div class="col-lg-4 col-sm-3">
 @if(!empty($booker->profilePic))
 <img src="/uploads/{{end($booker->profilePic)->pictureName}}" style="height:200px;width:100%;">
 @endif 
 <h3 class="text-center mt-3">{{$booker->name->firstName}}, 24</h3>
<form action="/viewProfileConfirm/{{$requestall->_id}}" method="POST">
  @csrf
  @method('PATCH')
  <input type="text" value="{{$requestall->daterId}}" name="bookerId" hidden>
  <button type="submit" name="pendingConfirm" value="approvedRequestConfirm" class="btn btn-basic w-100 mt-3">View Profile</button>
</form>



</div>
<div class="col-lg-8 col-sm-9">
  <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
   
  <div id="map"></div>

  <script>
    function initMap() {
      var chicago = new google.maps.LatLng({{$requestall->location->latitude}}, {{$requestall->location->longhitude}});

      var map = new google.maps.Map(document.getElementById('map'), {
        center: chicago,
        zoom: 19
      });

      var coordInfoWindow = new google.maps.InfoWindow();
      coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
      coordInfoWindow.setPosition(chicago);
      coordInfoWindow.open(map);

      map.addListener('zoom_changed', function() {
        coordInfoWindow.setContent(createInfoWindowContent(chicago, map.getZoom()));
        coordInfoWindow.open(map);
      });
    }

    var TILE_SIZE = 256;

    function createInfoWindowContent(latLng, zoom) {
      var scale = 1 << zoom;

      var worldCoordinate = project(latLng);

      var pixelCoordinate = new google.maps.Point(
          Math.floor(worldCoordinate.x * scale),
          Math.floor(worldCoordinate.y * scale));

      var tileCoordinate = new google.maps.Point(
          Math.floor(worldCoordinate.x * scale / TILE_SIZE),
          Math.floor(worldCoordinate.y * scale / TILE_SIZE));

      return [
        "{{$requestall->location->locationName}}",
    
      ].join('<br>');
    }

    // The mapping between latitude, longitude and pixels is defined by the web
    // mercator projection.
    function project(latLng) {
      var siny = Math.sin(latLng.lat() * Math.PI / 180);

      // Truncating to 0.9999 effectively limits latitude to 89.189. This is
      // about a third of a tile past the edge of the world tile.
      siny = Math.min(Math.max(siny, -0.9999), 0.9999);

      return new google.maps.Point(
          TILE_SIZE * (0.5 + latLng.lng() / 360),
          TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
    }
  </script>
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVuc0-ACKor2V2jUwh2Hhw3MXDDCOwnTo&callback=initMap">
  </script>

</div>
<div class="col-lg-4">
  <h5 class="mt-3"><strong>When: </strong> {{\Carbon\Carbon::parse($requestall->date)->isoFormat('MMMM Do YYYY')}}</h5>
  <h5 class="mt-3"><strong>What Time:</strong> {{date('h:i a', strtotime($requestall->time->startTime))}} TO {{date('h:i a', strtotime($requestall->time->endTime))}}</h5>
  <h5 class="mt-3"><strong>Price: $ </strong> {{$requestall->price}}</h5>
</div>
<div class="col-lg-6">
  <h5 class="mt-3"><strong>Where:</strong> {{$requestall->location->locationName}}
 <br>
 
 <h5><strong>Pay Now!</strong></h5>
 <div class="btn-group" role="group" aria-label="Third group">
    
      <script
      src="https://www.paypal.com/sdk/js?client-id=AbS1IYipTTajMd5VDZh6nnMqeRm58NGQEXhJigRaLB66jfcjjp8Jl9DwMndl__IG6T4XZtsjA4Lm052-">
    </script>
          
          <div id="paypal-button-container"></div>

    
          <script>
              paypal.Buttons({
                createOrder: function(data, actions) {
                  // Set up the transaction
                  return actions.order.create({
                    purchase_units: [{
                      amount: {
                        value: '{{$requestall->price}}'
                      }
                    }]
                  });
                }
              }).render('#paypal-button-container');
            </script>
    </div>  
</div>
</div>




@endif
@endforeach






</div>

</div>
</div>



</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(window).on('load',function(){
$('#myModal3').modal('show');
});
</script>
@endif
@endforeach
@foreach($requestsall as $requestall)
{{-- paid request modal --}}
@if($requestall->bookerId === Session::get('user')->_id && $requestall->status == "paidRequest")
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" >
<div class="mx-auto modal-dialog modal-xl" role="document">
<div class="modal-content" style="height:%;" >
<div class="modal-header w-100">
 <h3>Congratulations! Goodluck on your date!.</h3>
 <form action="/pendingConfirm/{{$requestall->_id}}" method="POST">
  @csrf
  @method('PATCH')
  <button type="submit" name="pendingConfirm" class="close" value="paidRequestConfirm">
    <span aria-hidden="true">&times;</span>
  </button>
 </form>
</div>
<div class="modal-body">
@foreach($usersall as $booker)
@if($booker->_id === $requestall->daterId)

<h4>{{$booker->name->firstName}}'s contact number is @if(!empty($booker->verification)){{$booker->verification->contact}}, You can contact him/her to know more details about your date Thank you!. You can view your date details on your datelist</h4>
@endif
@endif

@endforeach

</div>

</div>
</div>



</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(window).on('load',function(){
$('#myModal4').modal('show');
});
</script>
@endif
@endforeach

@foreach($requestsall as $requestall)
{{-- decline request modal --}}
@if($requestall->bookerId === Session::get('user')->_id && $requestall->status == "declineRequest")
<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" >
<div class="mx-auto modal-dialog modal-xl" role="document">
<div class="modal-content" style="height:%;" >
<div class="modal-header alert-danger w-100">
 <h3>Sorry! Your Date Was Decline!.</h3>
 <form action="/pendingConfirm/{{$requestall->_id}}" method="POST">
  @csrf
  @method('PATCH')
  <button type="submit" name="pendingConfirm" class="close" value="declineRequestConfirm">
    <span aria-hidden="true">&times;</span>
  </button>
 </form>
</div>
<div class="modal-body">
@foreach($usersall as $booker)
@if($booker->_id === $requestall->daterId)

<h4>Your Date To {{$booker->name->firstName}} Was Decline. Better Luck Next Time!</h4>
@endif


@endforeach

</div>

</div>
</div>



</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(window).on('load',function(){
$('#myModal5').modal('show');
});
</script>
@endif
@endforeach
@foreach($requestsall as $requestall)
{{-- paid request to booker --}}
@if($requestall->daterId === Session::get('user')->_id && $requestall->status == "paidRequestConfirm")
<div class="modal fade" id="myModal6" tabindex="-1" role="dialog" >
<div class="mx-auto modal-dialog modal-xl" role="document">
<div class="modal-content" style="height:%;" >
<div class="modal-header w-100">
 <h3>Congratulations! Booker Paid Your Date.</h3>
 <form action="/pendingConfirm/{{$requestall->_id}}" method="POST">
  @csrf
  @method('PATCH')
  <button type="submit" name="pendingConfirm" class="close" value="paidRequestDaterConfirm">
    <span aria-hidden="true">&times;</span>
  </button>
 </form>
</div>
<div class="modal-body">
@foreach($usersall as $booker)
@if($booker->_id === $requestall->bookerId)

<h4>{{$booker->name->firstName}}'s contact number is @if(!empty($booker->verification)){{$booker->verification->contact}}, You can contact him/her to know more details about your date Thank you!. You can view your date details on your datelist</h4>
@endif
@endif

@endforeach

</div>

</div>
</div>



</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(window).on('load',function(){
$('#myModal6').modal('show');
});
</script>
@endif
@empty
<div class="container">
<h3>No Dater Found</h3>
</div>
@endforelse
@endsection