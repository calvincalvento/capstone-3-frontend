@extends('layouts.app')

@section('content')

<div class="container ">

  <br>
  <h1>Pending Users</h1>
  @if(Session::get('approve'))
  <div class="alert alert-basic btn-basic alert-dismissible fade show" role="alert">
      <strong>Success!</strong> {{Session::get('approve')}}.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  @forelse($usersall as $userall)
  @if($userall->status == "user-nonverified")
  <div class="row">

      <div class="col-lg-4">
          @if(!empty ($userall->verification))
       
          <p>Name: <strong>{{$userall->name->firstName}}, {{$userall->name->lastName}} </strong></p>
          <p>Address:<strong> {{$userall->verification->address->blk}}</strong></p>
          <p>Birthday: <strong>{{\Carbon\Carbon::parse($userall->verification->birthday)->format('d/m/Y')}}</strong> </p>
          <p>Age:<strong> {{$userall->verification->age}}</strong> </p>
          <p>Contact: <strong> {{$userall->verification->contact}}</strong> </p>
          <p>Nationality: <strong> {{$userall->verification->nationality}}</strong> </p>
        
                
          @else
          <p>Name:<strong>{{$userall->name->firstName}}, {{$userall->name->lastName}}</strong></p>
          <p>Birthday:<strong> No answer yet</strong>  </p>
          <p>Age:<strong> No answer yet</strong>  </p>
          <p>Contact:<strong> No answer yet</strong>  </p>
          <p>Nationality:<strong> No answer yet</strong>  </p>
          
          <p>Valid Id:<strong> No answer yet</strong>  </p>
          <form action="/approveUser/{{$userall->_id}}" method="POST">
            @csrf
            @method('PATCH')
            <input type="text" value="user-verified-confirm" name="approveUser" hidden>
            <button type="submit" class="btn btn-success mt-3">Verify</button>
  
          </form>
<button type="button" class="btn btn-danger">Deny</button>

          @endif
      </div>
      <div class="col-lg-4">


          <p>Verification Id:</p>
        
          <img src="/verification/{{$userall->verification->validId}}" style="height:200px;width:100%;">

      </div>
      <div class="col-lg-4">
          <form action="/viewProfileUser/{{$userall->_id}}" method="POST">
            @csrf
            
            <button type="submit" class="btn btn-primary text-center w-100" >View Profile</button>
     </form>
        <form action="/approveUser/{{$userall->_id}}" method="POST">
          @csrf
          @method('PATCH')
          <input type="text" value="user-verified" name="approveUser" hidden>
          <button type="submit" class="btn btn-success mt-3 w-100">Verify</button>

        </form>
        <button type="button" class="btn btn-danger mt-3 w-100">Deny</button>
      </div>
     
     
    </div>
 
       
    
        
      
   
          @endif
          @empty
          <h1>No Pending User Found</h1>
        @endforelse
        
       
    
</div>


@endsection

