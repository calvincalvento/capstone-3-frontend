@extends('layouts.app')

@section('content')
<style>
 
  </style>
<div class="mb-5 mt-5 container  text-center py-5">
<h1 class="mt-3">FAQ</h1>
<hr>
<h3 class="font-weight-bold">What is UpDate all about?</h3>
<p>We make meet-ups simple and more rewarding for everyone. 
    Grab a buddy to hangout with or simply have a meal together
    Getting to know someone new is always exciting!</p>
    <br>
    <h3 class="font-weight-bold">Rules?</h3>
<p>No hard rules, just be polite and respectful. 

    There are strictly no forms of physical contact which will be tolerated.
    Only a friendly handshake and nothing more than that.
    
    If the rules are violated (such as inappropriate touching), your dates have the right to leave before the specified time ends and no refund is applicable.</p>
    <br>
    <h3 class="font-weight-bold">How do I make a booking?</h3>
    <p>Simply click on "Click To Book Me" button.

        It will prompt you to send a message via whatsapp to us.
        Once done, just wait for your booking confirmation while we liaise with your potential Maybe date!</p>
        <br>
    <h3 class="font-weight-bold">How do I make my payment?</h3>
    <p>How do I make my payment?</p>

        <br>
    <h3 class="font-weight-bold">Can I make Cash Payment on the day itself?</h3>
    <p>Payment have to be made upfront before a booking is confirmed. </p>

        <br>
    <h3 class="font-weight-bold">Will my date be the same person stated and shown in the profile?</h3>
    <p>Our members go through an interview process and meet us personally for the application.

        You can be assured that your date will be the same person.</p>

        <br>
    <h3 class="font-weight-bold">Is my privacy protected?</h3>
    <p>UpDate treats every customer's privacy confidential and will not disclose your personal information to any 3rd party or Social Media platform without your consent.</p>


</div>
@endsection