@extends('layouts.app')

{{-- 

@section('content')
<!-- Latest compiled and minified CSS -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap 4 DatePicker</title>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

<body>
  <div class="container">

    <input id="datepicker" width="312" />

    @include('partials.datePicker')

  </div>
    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4',
           
            value: '07/12/2019',
            
           disableDates:  function (date) {
               var disabled = [10,15,20,25];
               if (disabled.indexOf(date.getDate()) == -1 ) {
                   return true;
               } else {
                   return false;
               }
           }
        });
    </script>
   
    
     

  

</body>


</html>
@endsection --}}

@section('content')
    <!-- Latest compiled and minified CSS -->
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap 4 DatePicker</title>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
\

    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
     \
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
    </style>
  </head>
  <body>
    

    
    


   
   
<div class="container">
 <h1 class="text-center mb-5">Date Request</h1>
 @if($errors->any())
 <div class="container">
<ul class="list-group">
@foreach($errors->all() as $error)
<li class="alert alert-danger">{{ $error }}</li>
@endforeach
</ul>
<br>
 </div>       

@endif
 <div class="row">
        <div class="col-sm-12 col-lg-4">
          
          <div class="card" style="width: 100%;height:400px;">
                @if(!empty (($user->profilePic)))
            <img id="output" src="/uploads/{{end($user->profilePic)->pictureName}}" style="height:300px;" >
            @else
            <img id="output" src="https://www.sccpre.cat/mypng/full/363-3631746_profile-profile-picture-human-face-head-man-woman.png" style="height:300px;" >
            @endif
                <div class="card-body">
                  <h3 class="card-title text-center">{{$user->name->firstName}}, @if(!empty($user->verification)) {{$user->verification->age}} @endif</h3>
                 
                </div>
              </div>
        </div>
        <div class="col-lg-4 col-sm-12">
                <div class="card " style="width: 100%;height:400px;">
                        <div class="card-body">
                                <h3>About</h3>
                                <p>Lives in:@if(!empty($user->verification)) <strong class="text-capitalize"> {{$user->verification->address->city}}, {{$user->verification->address->province}}</strong> @endif</p>
                                @if(!empty($user->aboutMe))
                                <p>Work as: :  <strong style="text-transform:capitalize;">{{$user->aboutMe->work}}</strong></p>
                                  <p>Height: <strong style="text-transform:capitalize;">{{$user->aboutMe->height}}cm</strong></p>
                                  <p>Weight: <strong style="text-transform:capitalize;">{{$user->aboutMe->weight}}kg</strong></p>
                                  <p>Relationship: <strong style="text-transform:capitalize;">{{$user->aboutMe->relationship}}</strong> </p>
                                    <p>Smoke: <strong style="text-transform:capitalize;">{{$user->aboutMe->smoke}}</strong></p>
                                    <p>Drink: <strong style="text-transform:capitalize;">{{$user->aboutMe->drink}}</strong></p>
                                    <p>Gender: <strong style="text-transform:capitalize;">{{$user->verification->gender}}</strong></p>

                                    @else
                                    <p>Work as: : <strong style="text-transform:capitalize;">N/A</strong></p>
                                  <p>Height: <strong style="text-transform:capitalize;">N/A</strong></p>
                                  <p>Weight: <strong style="text-transform:capitalize;">N/A</strong></p>
                                  <p>Relationship: <strong style="text-transform:capitalize;">N/A</strong> </p>
                                    <p>Smoke: <strong style="text-transform:capitalize;">N/A</strong></p>
                                    <p>Drink: <strong style="text-transform:capitalize;">N/A</strong></p>
                                    @endif
                                    
                        </div>
                      </div>
         
          
        </div>
        <div class="col-lg-4 col-sm-12">
                <div class="card " style="width: 100%;height:400px;">
                        <div class="card-body">
                          <h3>Not Available Dates</h3>
                             @foreach($daterequests as $daterequest)
                             @if($daterequest->daterId == $user->_id && $daterequest->status == "paidRequestDaterConfirm" || $daterequest->status == "paidRequestConfirm" || $daterequest->status == "paidRequest"  )

                             @if(\Carbon\Carbon::parse($daterequest->date)->format('mdY') > \Carbon\Carbon::now()->format('mdY'))
                                  <p><strong class="text-capitalize">{{\Carbon\Carbon::parse($daterequest->date)->isoFormat('MMMM Do YYYY')}}</strong></p>
                                  @endif
                                  @endif
                          

                             
                             @endforeach
                             
                        </div>
                      </div>

                
              </div>
            
      
      </div>
<br>
      <h5>When do you want to date?</h5>
    
           

      <div class="row">
            <form method="POST" action="/bookRequestForm/{{$user->_id}}">
            @csrf
            <input type="text" width="312" value="{{Session::get('user')->_id}}" name="userId" hidden>
            <input id="datepicker" width="312" name="date">
              <div class="col col-lg-12">
         
      
      <label for="sel1">Start Time</label>
<select name="timestart" class="form-control">
    <option disabled selected value> -- select a time -- </option>
            <option value="00:00:00">12:00 am</option>
            <option value="01:00:00">1:00 am</option>
            <option value="02:00:00">2:00 am</option>
            <option value="03:00:00">3:00 am</option>
            <option value="04:00:00">4:00 am</option>
            <option value="05:00:00">5:00 am</option>
            <option value="06:00:00">6:00 am</option>
            <option value="07:00:00">7:00 am</option>
            <option value="08:00:00">8:00 am</option>
            <option value="09:00:00">9:00 am</option>
            <option value="10:00:00">10:00 am</option>
            <option value="11:00:00">11:00 am</option>
            <option value="12:00:00">12:00 pm</option>
            <option value="13:00:00">1:00 pm</option>
            <option value="14:00:00">2:00 pm</option>
            <option value="15:00:00">3:00 pm</option>
            <option value="16:00:00">4:00 pm</option>
            <option value="17:00:00">5:00 pm</option>
            <option value="18:00:00">6:00 pm</option>
            <option value="19:00:00">7:00 pm</option>
            <option value="20:00:00">8:00 pm</option>
            <option value="21:00:00">9:00 pm</option>
            <option value="22:00:00">10:00 pm</option>
            <option value="23:00:00">11:00 pm</option>
 </select>
 <label for="sel1">End Time:</label>
<select name="timestop" class="form-control">
    <option disabled selected value> -- select a time -- </option>
            <option value="00:00:00">12:00 am</option>
            <option value="01:00:00">1:00 am</option>
            <option value="02:00:00">2:00 am</option>
            <option value="03:00:00">3:00 am</option>
            <option value="04:00:00">4:00 am</option>
            <option value="05:00:00">5:00 am</option>
            <option value="06:00:00">6:00 am</option>
            <option value="07:00:00">7:00 am</option>
            <option value="08:00:00">8:00 am</option>
            <option value="09:00:00">9:00 am</option>
            <option value="10:00:00">10:00 am</option>
            <option value="11:00:00">11:00 am</option>
            <option value="12:00:00">12:00 pm</option>
            <option value="13:00:00">1:00 pm</option>
            <option value="14:00:00">2:00 pm</option>
            <option value="15:00:00">3:00 pm</option>
            <option value="16:00:00">4:00 pm</option>
            <option value="17:00:00">5:00 pm</option>
            <option value="18:00:00">6:00 pm</option>
            <option value="19:00:00">7:00 pm</option>
            <option value="20:00:00">8:00 pm</option>
            <option value="21:00:00">9:00 pm</option>
            <option value="22:00:00">10:00 pm</option>
<option value="23:00:00">11:00 pm</option>
            </select>
             
            <h5 class="mt-3">Price Per Hour:$ {{number_format($user->price, 2)}}</h5>
            <h5 id="difference" class="mt-3"> </h5>
            <h5 id="datePrice" class="mt-3"> </h5>

            <input type="text" id="price" value="{{$user->price}}" class="mb-4" hidden>
            <input type="text" id="overallPrice" name="overallPrice" class="mb-4" hidden>
                <input id="pac-input" class="controls" type="text"
                    placeholder="Enter a location" >
                <div id="type-selector" class="controls">
                  <input type="radio" name="type" id="changetype-all" checked="checked">
                  <label for="changetype-all">All</label>
                </div>
               
                <br>
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">
                <input type="hidden" name="location" id="location">
            
            </div>
        </div><!--End of row-->
        <div id="map" style="height: 300px;width: 100%"></div>
        <input type="submit" name="submit" value="DATE NOW!!!" class="form-control btn btn-danger mt-3 mb-3">
    </form>
</div><!--End of conatiner-->
    <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">


      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {
            lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();

          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
var item_Lat =place.geometry.location.lat()
var item_Lng= place.geometry.location.lng()
var item_Location = place.formatted_address;
//alert("Lat= "+item_Lat+"_____Lang="+item_Lng+"_____Location="+item_Location);
        $("#lat").val(item_Lat);
        $("#lng").val(item_Lng);
        $("#location").val(item_Location);
          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVuc0-ACKor2V2jUwh2Hhw3MXDDCOwnTo&libraries=places&callback=initMap"
        async defer></script>
        <script>
                $('#datepicker').datepicker({
                    uiLibrary: 'bootstrap4',
                   
                    value: '07/12/2019',
                    
                   disableDates:  function (date) {
                       var disabled = [  
                         @foreach($daterequests as $daterequest)
                         @if($daterequest->daterId == $user->_id && $daterequest->status == "paidRequest" || $daterequest->status == "paidRequestConfirm" || $daterequest->status == "paidRequestDaterConfirm"  )
                         @if(\Carbon\Carbon::parse($daterequest->date)->format('mdY') > \Carbon\Carbon::now()->format('mdY'))
                                 {{\Carbon\Carbon::parse($daterequest->date)->format('d')}},
                                 @endif
                             

                             @endif
                             @endforeach
                             ];
                             
                       if (disabled.indexOf(date.getDate()) == -1 ) {
                           return true;
                       } else {
                           return false;
                       }
                   }
                });
            </script>
            <script>
            $(document).ready(function() {    
    function calculateTime() {
            //get values
            var valuestart = $("select[name='timestart']").val();
            var valuestop = $("select[name='timestop']").val();
              
             //create date format          
             var timeStart = new Date("01/01/2007 " + valuestart).getHours();
             var timeEnd = new Date("01/01/2007 " + valuestop).getHours();
             
             var hourDiff = timeEnd - timeStart;    
             let price =document.getElementById("price").value;       
             $("#difference").html("<b>Dating Hours:</b> " + hourDiff + " hrs" );
             document.getElementById("overallPrice").value = hourDiff * price;   
             $("#datePrice").html("<b>Total Price:</b> " + hourDiff * price + " $" );
    }
    $("select").change(calculateTime);
    calculateTime();
    });
                    </script>
@endsection