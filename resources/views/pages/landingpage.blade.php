<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css" >
    <link rel="stylesheet" href="/css/owl.carousel.css" >
    <link rel="stylesheet" href="/css/owl.theme.default.min.css" >
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text&display=swap" rel="stylesheet">
    <link href="css/animated.css" rel="stylesheet">
    
   
    <title>Update</title>
  </head>
  <body>

      <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3&appId=384853122423827&autoLogAppEvents=1"></script>

      <div id="load"></div>

     


       
    {{-- login --}}
    <div class="wew wow fadeIn delay-1s" id="landing">
    <div class="landing">
        <div class="container-fluid">
        
            <div class="row">
              
              <div class="col-sm-12 col-lg-4 offset-lg-1 col-md-5 offset-md-1 wow fadeIn card-landing px-5 pt-3">
                
                
                        <img src="/images/logo.png" class="logo-landing wow fadeIn " >
                  
               
                  
                <h2 class="text-center">Browse.Book.Date</h2>
                <p class="text-center">Connecting people that wants to explore and meet different person</p>
                @if(Session::get('password'))
                <div class="alert alert-danger" role="alert">
                    {{Session::get('password')}}
                  </div>
                  @endif
                <form action="/login" method="POST">
                  @csrf
                    <div class="form-group">
                      <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="&#xf007; Enter Email" style="font-family:Arial, FontAwesome" >
                      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                      
                      <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="&#xf023; Password" style="font-family:Arial, FontAwesome">
                      <button type="submit" class="btn btn-danger mt-2 w-100">Take a Chance</button>
                    </form>

                      <button type="button" class="btn btn-light mt-2 w-100 font-weight-bold"><img src="http://pngimg.com/uploads/google/google_PNG19635.png" style="height: 25px;margin-top:-3px;"> Sign In Via Google</button>
                      <button type="button" class="btn btn-light mt-2 w-100 font-weight-bold"><img src="https://cdn2.iconfinder.com/data/icons/popular-social-media-flat/48/Popular_Social_Media-01-512.png" style="height: 25px;margin-top:-3px;"> Sign In Via Facebook</button>
                      <div class="fb-login-button" style="" data-width="" data-size="large" data-button-type="login_with" data-auto-logout-link="false" data-use-continue-as="false"></div>
                      <p id="emailHelp" class="form-text text-muted text-center mt-5">Don't have an account yet?<button type="button" class="btn btn-basic" data-toggle="modal" data-target="#exampleModal1">
                          Sign Up Here
                        </button>
                     
                        @include('partials.register')
                        </p>
                    </div>
                    
                    <button>  
              </div>
            </div>
          </div>
    
    </div>
    
{{-- date cards --}}
     @include('partials.owlcarousel')
     
  

     {{-- SERVICES --}}
  

     <div class="container-fluid mt-5 con-service">
        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-3 text-center">
              <p><i class="fas fa-shield-alt service-icon"></i></p>
              <h1>Protection</h1>
              <p class="text-muted">Your account and details is safe with us.</p>
           
          </div>
          <div class="col-sm-12 col-md-6 col-lg-3 text-center">
              
                  <p><i class="fas fa-money-bill-alt service-icon"></i></p>
                  <h1>Refund</h1>
                  <p class="text-muted">Guaranteed money pay back when the book is cancelled.</p>
          </div>
              
          <div class="col-sm-12 col-md-6 col-lg-3 text-center">
           
              <p><i class="fas fa-user-shield service-icon"></i></p>
              <h1>Trusted</h1>
              <p class="text-muted">We provide trusted and legit people that you can transact with.</p>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-3 text-center">
            
              <p><i class="fas fa-phone service-icon"></i></p>
              <h1>Support</h1>
              <p class="text-muted">Fast Response on our admins whenever you have concerns.</p>
          </div>
        </div>
      </div>
      
      {{-- jumbotron --}}

      <div class="text-center mt-5 p-5" style="background-color:#FAF2E8;">
          <h1 class="display-4"><img src="/images/alone.jpg" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/alone1.jpg" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/alone2.jpg" style="height:50px;width:50px;border-radius:50%;">
          </h1>
          <p class="lead text-muted">Daniel, Baguio</p>
          <div class="container">
          <p class="font-italic">"I tried many dating application but sadly i can't match,talk or date to someone maybe because my pictures are ugly.I'm a lonely person, But when I use this website I've met different kinds of people and girls that I want to."</p>
          </div>
        </div>
        <div class="party pt-5">

            <div class="container p-5 bg-transparent text-white text-center">
                <h2 style="font-family: 'Crimson Text', serif;">You can date whoever you want and <br> receive a lot of attention to our members.</h2>
                
                <p> Join the easy way </p>
               
                <p class="lead">
                  <a class="btn btn-basic btn-md" href="#" role="button">Create an account</a>
                </p>
              </div>
        </div>

        <footer class="text-center mt-5">
          <h5>follow us on</h5>
          <h1><img src="/images/facebook.png" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/twitter.png" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/gmail.png" style="height:50px;width:50px;border-radius:50%;"> <img src="/images/youtube.png" style="height:50px;width:50px;border-radius:50%;"></h1>
          <br>
          <p class="text-muted">Copyright CalvinCalvento 2019. All rights reserved
        </footer>
    </div>
  
       
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
 
     <script>
      document.onreadystatechange = function () {
  var state = document.readyState
  if (state == 'interactive') {
       document.getElementById('contents').style.visibility="hidden";
  } else if (state == 'complete') {
      setTimeout(function(){
         document.getElementById('interactive');
         document.getElementById('load').style.visibility="hidden";
         document.getElementById('contents').style.visibility="visible";
      },1000);
  }
}
</script> 

 
  
    


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    {{-- owl carousel --}}
    {{-- <script src="{{ asset('js/jquery.min.js') }}"></script> --}}
    <script src="{{ asset('js/owl.carousel.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>

  


<!-- Latest compiled JavaScript -->
<script type="text/javascript" language="JavaScript">
    
  //--------------------------------
  // This code compares two fields in a form and submit it
  // if they're the same, or not if they're different.
  //--------------------------------
  function checkEmail(theForm) {
      if (theForm.password.value != theForm.confirmpassword.value)
      {
        document.getElementById("bobo").innerHTML = "Confirm Password and Password does not match!";
          return false;
      } else {
          return true;
      }
  }
  //-->
  </script>
 
   
   
    <script>
        window.onload = function(){
 $("#landing").removeClass("wew");
        };
        </script>


<script>
  $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        999:{
            items:3,
            nav:true
        },
        1000:{
            items:7,
            nav:true,
            loop:true
        }
    }
})
  </script>
  <script src="js/wow.min.js"></script>
   <script>
      new WOW().init();
      </script>
   
   <script>



     </script>
  </body>
</html>