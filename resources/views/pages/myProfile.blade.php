@extends('layouts.app')

@section('content')


<div class="container mb-5">
    @if(Session::get('approveNotif'))
    <div class="alert alert-basic btn-basic alert-dismissible fade show" role="alert">
        <strong>Success!</strong> {{Session::get('approveNotif')}}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif
    <div class="row">
    @if(!empty($user->coverPic))
      <div class="col-lg-12 cover-photo d-flex px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2) ), url('/uploads/{{$user->coverPic}}');">
      @else
      <div class="col-lg-12 cover-photo d-flex px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2) ), url('https://images.pexels.com/photos/1417255/pexels-photo-1417255.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=750&w=1260');">
      @endif
      
          <div class="ml-auto mt-5">
        
          <form action='/coverPic/{{$user->_id}}' method="POST"  enctype="multipart/form-data" class="contact100-form validate-form">
            @csrf
            @method('PATCH')
         
         

          <input type="file" accept="image/x-png,image/gif,image/jpeg" name="coverPic" id="ssfile" class="inputfile mb-5"/>
          <label class="btn btn-dark uploadPic" for="ssfile">Change Cover Picture</label>
          <div class="ml-5">
          <a href="/myProfile" > <button type="button"  class="btn btn-light" id="Cancel1" style="display:none">Cancel</button></a>
              <button type="submit" class="btn btn-primary" id="saveChanges1" style="display:none">Change Cover</button>
              </div>
             </form>
          </div>
        
     
      </div>
      </div>
    <div class="row bg-light">
    <div class="col-lg-3 col-md-3 col-sm-12">
       
        <a data-toggle="modal" data-target="#exampleModal" style="cursor:pointer;">
         @if(!empty (($user->profilePic)))
            <img id="output" src="/uploads/{{end($user->profilePic)->pictureName}}" class="w-100  profile-pic">
            @else
            <img id="output" src="https://www.sccpre.cat/mypng/full/363-3631746_profile-profile-picture-human-face-head-man-woman.png" class="w-100  profile-pic">
            @endif
           </a>
           @include('partials.profilepicture')
         
           <form action='/profilePic/{{$user->_id}}' method="POST"  enctype="multipart/form-data" class="contact100-form validate-form">
            @csrf
            @method('PATCH')
         
         

          <input type="file" accept="image/x-png,image/gif,image/jpeg" name="profilePic" id="sfile" class="inputfile"  onchange="loadFile(event)"  />
          <label class="btn btn-dark uploadPic" for="sfile">Upload Picture</label>
          <a href="/myProfile" > <button type="button"  class="btn btn-light" id="Cancel" style="display:none">Cancel</button></a>
              <button type="submit" class="btn btn-primary" id="saveChanges" style="display:none">Upload Photo</button>
             </form>


         
      </div>
      <div class="col-lg-9 col-md-9 col-sm-12 text-white font-weight-bold" style="margin-top:-50px;">
          <h3 class="username">{{$user->name->firstName}}, {{$user->name->lastName}}  
            @if($user->status == "user-nonverified")
            <a href="/verifyAccount" type="button" class="btn btn-dark bg-danger">
              Verify Your Account!</i></a>
              @elseif (!empty($user->verification))
              , {{$user->verification->age}}
              <!-- edit caption trigger modal -->


<!-- edit caption Modal -->

            @endif <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModaleditCaption">
                Edit Caption
              </button></h3>

              <div class="modal fade" id="exampleModaleditCaption" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form action="/editCaption/{{$user->_id}}" method="POST">
                        @csrf
                        @method('PATCH')
                         <div class="form-group">
                    <label for="exampleFormControlTextarea1" class="text-dark">Enter Caption:</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="'Describe Yourself'" name="editCaption" minlength="10" required></textarea>
                  </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
            
            @if(!empty($user->caption))
          <h4 class="text-dark text-center font-weight-bold" style="margin-top:70px;">"{{$user->caption}}"</h4>
          @else
          <h4 class="text-dark text-center font-weight-bold" style="margin-top:70px;">"I like Black black looks good looking good means no dandruff"</h4>
          @endif
      <div>
    </div>
    </div>
  </div>
  @if(Session::get('editAbout'))
  <div class="alert alert-danger  alert-dismissible fade show" role="alert">
      <strong>Success!</strong> {{Session::get('editAbout')}}.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <div class="row mt-5">
  
      <div class="col-sm-12 col-lg-4 ">


          <div class="card" style="min-height:400px;background-color: rgba(204, 98, 91, 0.4);">
              <h5 class="card-header">My Interest <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModalInterest">
                  <i class="fas fa-edit"></i>
                </button></i> </h5>
              
              <div class="card-body">
              <h5>
                @forelse($user->myInterest as  $interest)
                <h4>{{$interest->interestName}},</h4>
                @empty
                no interest yet
                @endforelse
                <h5>
              </div>
            </div>
      </div>
      <div class="col-sm-12 col-lg-4">
          <div class="card" style="min-height:400px;background-color: rgba(204, 98, 91, 0.4) !important;">
              <h5 class="card-header">About Me <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModalAboutMe">
                <i class="fas fa-edit"></i>
              </button></h5>
          
              <div class="card-body">
                @if(!empty($user->aboutMe))
                  <p>Lives in: <strong style="text-transform:capitalize;">{{$user->aboutMe->city}}, {{$user->aboutMe->country}}</strong></p>
              <p>Work as: <strong style="text-transform:capitalize;">{{$user->aboutMe->work}}</strong></p>
                  <p>Height: <strong>{{$user->aboutMe->height}}cm </strong></p>
                  <p>Weight: <strong>{{$user->aboutMe->weight}}kg </strong></p>
                  <p>Relationship: <strong style="text-transform:capitalize;">{{$user->aboutMe->relationship}}</strong> </p>
                  <p>Smoke: <strong style="text-transform:capitalize;">{{$user->aboutMe->smoke}}</strong></p>
                  <p>Drink: <strong style="text-transform:capitalize;">{{$user->aboutMe->drink}}</strong></p>
                @else
            
                
                <p>Lives in:</p>
                <p>Work as: <strong style="text-transform:capitalize;"></strong></p>
                  <p>Height: <strong></strong></p>
                  <p>Weight: <strong></strong></p>
                  <p>Relationship: <strong style="text-transform:capitalize;"></strong> </p>
                    <p>Smoke: <strong style="text-transform:capitalize;"></strong></p>
                    <p>Drink: <strong style="text-transform:capitalize;"></strong></p>
                
                    @endif
              </div>
            </div>
      </div>
      <div class="col-sm-12 col-lg-4">
          <div class="card" style="min-height:400px;background-color: rgba(204, 98, 91, 0.4) !important;">
              <h5 class="card-header">I'm Looking for <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#exampleModalLookingFor">
                  <i class="fas fa-edit"></i>
                </button></i></h5>
              <div class="card-body">
                @if(!empty ($user->lookingFor))
              <h5>Looking for a <strong>{{$user->lookingFor->gender}}</strong></h5>
<br>
              <h4>"{{$user->lookingFor->description}}"</h4>
              @else
              <h5>Looking for a <strong>Everybody</strong></h5>
              <br>
                            <p>"N/A"</p>
                            @endif
              
          
                          </div>
            </div>
      </div>
    </div>
   




@if(Session::get('verify'))
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" style="margin-top:200px;">
      <div class="modal-content">
          <div class="modal-header">
             
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
        <div class="modal-body">
        
          <div class="alert alert-danger" role="alert">
              <p class="text-center">{{Session::get('verify')}}</p>
            </div>
        </div>
      
      </div>
     
    </div>
 
  
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
          $('#myModal').modal('show');
      });
  </script>
</div>
@endif
<h3 class="mt-3">You might like this</h3>

<div class="row mt-3 mb-5">
    
                          
                  
@foreach($usersall as $userall)

<div class="col-lg-4 mt-3 ">
      
      @if(!empty($userall->profilePic))
<div class="card users px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5) ), url('/uploads/{{end($userall->profilePic)->pictureName}}');">
      @else
      <div class="card users px-5" style="background-image:linear-gradient( rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.5) ), url('https://images.pexels.com/photos/1417255/pexels-photo-1417255.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&h=750&w=1260');">
              @endif
              <div class="card-body " style="position:relative;">
                <h4 class="text-capitalize card-title p-2 text-white text-center "  style="position:absolute;bottom:50px;left:0;right:0;border-radius:10px; "><strong>{{$userall->name->firstName}}</strong> @if(!empty($userall->verification))
                              , {{$userall->verification->age}}
                              @else , N/A
              @endif
       </h4>
       <form action="/viewProfileUser/{{$userall->_id}}" method="POST">
              @csrf
              
              <button type="submit" class="btn btn-danger text-center w-100" style="position:absolute;bottom:15px;left:0">Date now! <i class="fas fa-heart text-white"></i></button>
       </form>
              </div>
            </div>
</div>

@endforeach
</div>
<script>
    var loadFile = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
    };
  </script>
  <script>
    var loadFile1 = function(event) {
      var output = document.getElementById('output');
      output.src = URL.createObjectURL(event.target.files[0]);
    };
  </script>
  @include('partials.editModals')
@endsection