@extends('layouts.app')

@section('content')

<style>
  @import url('https://fonts.googleapis.com/css?family=Yantramanav:100,300');

/* ------------- */
/* GLOBAL STYLES */
/* ------------- */


.container {
  max-width: 1170px;
  margin-left: auto;
  margin-right: auto;
  padding: 1em;
}

ul {
  list-style: none;
  padding: 0;
}

.brand {
  text-align: center;
	font-weight: 300;
	text-transform: uppercase;
	letter-spacing: 0.1em;
}


.wrapper {
  box-shadow: 0 0 20px 0 rgba(57, 82, 163, 0.7);
}

.wrapper > * {
  padding: 1em;
}

/* ------------------- */
/* COMPANY INFORMATION */
/* ------------------- */

.company-info {
  background: #F69F98;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
}

.company-info h3,
.company-info ul {
  text-align: center;
  margin: 0 0 1rem 0;
}

/* ------- */
/* CONTACT */
/* ------- */

.contact {
  background: #Fdd5c8;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
}

/* ---- */
/* FORM */
/* ---- */

.contact form {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
}

.contact form label {
  display: block;
}

.contact form p {
  margin: 0;
}

.contact form .full {
  grid-column: 1 / 3;
}

.contact form button,
.contact form input,
.contact form textarea {
  width: 100%;
  padding: 1em;
  border: solid 1px #627EDC;
  border-radius: 4px;
}

.contact form textarea {
  resize: none;
}

.contact form button {
  background: #dc3545;
  border: 0;
  color: #e4e4e4;
  text-transform: uppercase;
  font-size: 14px;
  font-weight: bold;
}

.contact form button:hover,
.contact form button:focus {
  background: #3952a3;
  color: #ffffff;
  outline: 0;
  transition: background-color 2s ease-out;
}

/* ------------- */
/* MEDIA QUERIES */
/* ------------- */

@media only screen and (min-width: 700px) {
  .wrapper {
    display: grid;
    grid-template-columns: 1fr 2fr;
  }

  .wrapper > * {
    padding: 2em;
  }

  .company-info {
    border-radius: 4px 0 0 4px;
  }

  .contact {
    border-radius: 0 4px 4px 0;
  }

  .company-info h3,
  .company-info ul,
  .brand {
    text-align: left;
  }
}

  </style>

<div class="backgrounds">
<div class="mt-5 container">

    
  
    <div class="wrapper">
  
      <!-- COMPANY INFORMATION -->
      <div class="company-info">
        <h3>Phoenix Web Design</h3>
  
        <ul>
          <li><i class="fa fa-road"></i> 44 Main Street</li>
          <li><i class="fa fa-phone"></i> (555) 555-5555</li>
          <li><i class="fa fa-envelope"></i> test@phoenix.com</li>
        </ul>
      </div>
      <!-- End .company-info -->
  
      <!-- CONTACT FORM -->
      <div class="contact">
        <h3>E-mail Us</h3>
  
        <form id="contact-form">
  
          <p>
            <label>Name</label>
            <input type="text" name="name" id="name" required>
          </p>
  
          <p>
            <label>Company</label>
            <input type="text" name="company" id="company">
          </p>
  
          <p>
            <label>E-mail Address</label>
            <input type="email" name="email" id="email" required>
          </p>
  
          <p>
            <label>Phone Number</label>
            <input type="text" name="phone" id="phone">
          </p>
  
          <p class="full">
            <label>Message</label>
            <textarea name="message" rows="5" id="message"></textarea>
          </p>
  
          <p class="full">
            <button type="submit" class="btn btn-danger">Submit</button>
          </p>
  
        </form>
        <!-- End #contact-form -->
      </div>
      <!-- End .contact -->
  
    </div>
    <!-- End .wrapper -->
  </div>
  <!-- End .container -->
</div>

  @endsection