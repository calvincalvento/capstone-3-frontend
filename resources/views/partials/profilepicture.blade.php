@if(!empty (($user->profilePic)))
<!-- Button trigger modal -->

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="margin-top:200px;" role="document">
      <div class="modal-content " >
      
        <div class="modal-body">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>   
           
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" style="height:500px;" src="/uploads/{{end($user->profilePic)->pictureName}}" alt="First slide">
                  </div>
                    @foreach(array_reverse(array_slice($user->profilePic,0, -1)) as $name)
                  
                    <div class="carousel-item">
                      <img class="d-block w-100" style="height: 500px" src="/uploads/{{$name->pictureName}}" alt="Second slide">
                    </div>
                @endforeach
               
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
        </div>
      
      </div>
    </div>
  </div>


@endif


