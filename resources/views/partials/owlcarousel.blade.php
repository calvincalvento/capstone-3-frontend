<div class="owl-carousel owl-theme mt-5 container-fluid px-5">
    @foreach($usersall as $userall)
    @if($userall->status == "userDate")
    <div class="item"><div class="card p-4" style="width: 100%;border:none; background-color: #f5eeee">
        <img class="card-img-top" src="/uploads/{{end($userall->profilePic)->pictureName}}" alt="Card image cap" style="border-radius:50%;height:190px;" class="pd-5">
        <div class="card-body">
          <h5 class="card-title text-center text-capitalize">{{$userall->name->firstName}}, {{$userall->verification->age}}</h5>
          <p class="card-text text-center text-muted text-capitalize">{{$userall->verification->address->city}}, {{$userall->verification->country}}</p>
          <a href="#" class="btn btn-light w-100">Hello!</a>
        </div>
      </div></div>
      @endif
      @endforeach
    
          
          
   
</div>
