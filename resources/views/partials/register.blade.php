<div class="modal " id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Sign Up</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
            @if($errors->any())
            <div class="container">
    <ul class="list-group">
      @foreach($errors->all() as $error)
        <li class="alert alert-danger">{{ $error }}</li>
        @endforeach
        </ul>
        <br>
            </div>       
    
@endif
            <form action="/register" method="POST" onsubmit="return checkEmail(this);">
              @csrf
              <label for="firstname">Name</label>
                <div class="row">
                  <div class="col">
                    <input type="text" class="form-control" name="firstName" placeholder="First name">
                  </div>
                  <div class="col">
                    <input type="text" class="form-control" name="lastName" placeholder="Last name">
                  </div>
                </div>
                <div class="form-group mt-2">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="&#xf0e0; Enter Email" style="font-family:Arial, FontAwesome">
                   
                  </div>
                
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="&#xf023; Password" style="font-family:Arial, FontAwesome">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputPassword1">Confirm Password</label>
                      <input type="password" class="form-control" id="confirmpassword" name="confirmpassword" placeholder="&#xf023; Confirm Password" style="font-family:Arial, FontAwesome">
                      <small id="bobo" class="form-text text-danger" style="font-size:15px;"></small>
                    </div>
              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-basic">Create Now</button>
        </form>
        </div>
      </div>
    </div>
  </div>
  @if($errors->any())
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(window).on('load',function(){
          $('#exampleModal1').modal('show');
      });
  </script>
  
  @endif