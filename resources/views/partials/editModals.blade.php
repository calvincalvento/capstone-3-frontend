
<!-- My interest Modal -->
<div class="modal fade" id="exampleModalInterest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">My Interest</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/myInterest/{{$user->_id}}" method="POST">
          @csrf
          @method('PATCH')
          <div class="form-group">
          
              <input type="test" name="myInterest" class="form-control" id="exampleInputEmail1"  placeholder="Enter your interest here">
              <small id="emailHelp" class="form-text text-muted">Your hobbies, likes, sports and etc.</small>
            </div>
          </div>
          <button type="submit" class="btn btn-dark w-50 mx-auto mb-3">Save changes</button>
        </form>
    
    </div>
  </div>
</div>

<!-- About me Modal -->

<div class="modal fade" id="exampleModalAboutMe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">About Me</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/aboutme/{{$user->_id}}" method="POST">
          @csrf
          @method('PATCH')

          <div class="row">
           
              </div>

          <div class="row">
            <div class="col">
                <label for="Height">Height</label>
                @if(!empty($user->aboutMe->height))
                <input type="number" value="{{$user->aboutMe->height}}" id="Height" class="form-control" name="Height" placeholder="Height in cm" required>
                @else
                <input type="number" id="Height" class="form-control" name="Height" placeholder="Height in cm" required>
                @endif
              </div>
              <div class="col">
                <label for="Weight">Weight</label>
                @if(!empty($user->aboutMe->weight))
                <input type="number" value="{{$user->aboutMe->height}}" id="Weight" class="form-control" name="Weight" placeholder="Weight in Kg" required>
                @else
                <input type="number" id="Weight" class="form-control" name="Weight" placeholder="Weight in Kg" required>
                @endif
              </div>
            </div>
            <div class="form-group mt-2">
                <label for="Relationship ">Relationship</label>
                <select class="form-control" id="Relationship" name="Relationship" required>
                  @if(!empty($user->aboutMe->relationship))
                  <option>{{$user->aboutMe->relationship}}</option>
                  @endif
                  <option>Single</option>
                  <option>Married</option>
                  <option>Widowed</option>
                  <option>In a relationship</option>
                </select>
              </div>
              <div class="form-group mt-2">
                  <label for="Smoke">Smoke</label>
                  <select class="form-control" id="Smoke" name="Smoke">
                      @if(!empty($user->aboutMe->smoke))
                      <option>{{$user->aboutMe->smoke}}</option>
                      @endif
                    <option>Yes</option>
                    <option>No</option>
                    <option>Never</option>
                    <option>Sometimes</option>
                  </select>
                </div>
                <div class="form-group mt-2">
                    <label for="Drink">Drink</label>
                    <select class="form-control" id="Drink" name="Drink">
                        @if(!empty($user->aboutMe->drink))
                        <option>{{$user->aboutMe->drink}}</option>
                        @endif
                      <option>Yes</option>
                      <option>No</option>
                      <option>Never</option>
                      <option>Occasionally</option>
                    </select>
                  </div>
              <div class="form-group">
                  <label for="Work">Work as</label>
                  @if(!empty($user->aboutMe->work))
                  <input type="text"name="Work" value="{{$user->aboutMe->work}}" class="form-control" id="Work" placeholder="What's your work" required >
                  @else
                  <input type="text"name="Work" class="form-control" id="Work" placeholder="What's your work" required >
                  @endif

                </div>
             
      </div>
     
          <button type="submit" class="btn btn-dark w-50 mx-auto mb-3">Save changes</button>
        </form>
     
    </div>
  </div>
</div>

<!--lookingfor Modal -->
<div class="modal fade "  id="exampleModalLookingFor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">What are you looking for?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="/lookingFor/{{$user->_id}}" method="POST">
            @csrf
            @method('PATCH')
              <div class="form-group">
                  <label for="exampleFormControlSelect1">Who you're looking?</label>
                  <select class="form-control" name="Gender" id="exampleFormControlSelect1">
                    @if(!empty($user->lookingFor->gender))
                    <option>{{$user->lookingFor->gender}}</option>
                    @endif
                    <option>Men</option>
                    <option>Women</option>
                    <option>Men and Women</option>
                    
                  </select>
                  <div class="form-group">
                      @if(!empty($user->lookingFor->description))
                      <label for="exampleFormControlTextarea1">Description</label>
                  <textarea class="form-control" name="Description" id="exampleFormControlTextarea1" rows="3" placeholder='"Message"'>{{$user->lookingFor->description}}</textarea>
                  @else
                  <label for="exampleFormControlTextarea1">Description</label>
                  <textarea class="form-control" name="Description" id="exampleFormControlTextarea1" rows="3" placeholder='"Message"'></textarea>
                </div>
                @endif
              </div>
                  
                      <button type="submit" class="btn btn-dark w-50 mb-3" style="margin-left:100px;">Save changes</button>
                    </form>
     
    </div>
  </div>
</div>
</div>

<!--lookingfor Modal -->
<div class="modal fade "  id="exampleModaleditProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" >
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">What are you looking for?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form action="/lookingFor/{{$user->_id}}" method="POST">
              @csrf
              @method('PATCH')
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Who you're looking?</label>
                    <select class="form-control" name="Gender" id="exampleFormControlSelect1">
                      @if(!empty($user->lookingFor->gender))
                      <option>{{$user->lookingFor->gender}}</option>
                      @endif
                      <option>Men</option>
                      <option>Women</option>
                      <option>Men and Women</option>
                      
                    </select>
                    <div class="form-group">
                        @if(!empty($user->lookingFor->description))
                        <label for="exampleFormControlTextarea1">Description</label>
                    <textarea class="form-control" name="Description" id="exampleFormControlTextarea1" rows="3" placeholder='"Message"'>{{$user->lookingFor->description}}</textarea>
                    @else
                    <label for="exampleFormControlTextarea1">Description</label>
                    <textarea class="form-control" name="Description" id="exampleFormControlTextarea1" rows="3" placeholder='"Message"'></textarea>
                  </div>
                  @endif
                </div>
                    
                        <button type="submit" class="btn btn-dark w-50 mb-3" style="margin-left:100px;">Save changes</button>
                      </form>
       
      </div>
    </div>
  </div>
</div>


