<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@landing');
// user
Route::post('/register', 'UserController@store');
Route::get('/homepage', 'UserController@index');
Route::post('/homepage/search', 'UserController@search')->middleware('auth');
// login logout
Route::post('/login', 'UserController@login');
Route::get('/logout', 'UserController@logout');
Route::get('/faqs', 'Controller@faqs');
Route::get('/contactUs', 'Controller@contactUs');
// myprofle
Route::get('/myProfile', 'ProfileController@index')->middleware('auth');
// verify page


// upload profile picture
Route::patch('/profilePic/{id}', 'ProfileController@profilePic')->middleware('auth');
Route::patch('/coverPic/{id}', 'ProfileController@coverPic')->middleware('auth');

Route::patch('/joinUs/{id}', 'ProfileController@joinUs')->middleware('auth');
//edit about me
Route::patch('/aboutme/{id}', 'ProfileController@editAbout')->middleware('auth');
// edit lookin for
Route::patch('/lookingFor/{id}', 'ProfileController@lookingFor')->middleware('auth');
//edit caption
Route::patch('/editCaption/{id}', 'ProfileController@editCaption')->middleware('auth');
//edit My interest
Route::patch('/myInterest/{id}', 'ProfileController@myInterest')->middleware('auth');
// verify account
Route::patch('/verifyAccount/{id}', 'VerificationController@verifyAccount')->middleware('auth');
Route::get('/verifyAccount', 'VerificationController@index')->middleware('auth');

// vuew dater profile
Route::get('/viewProfile', 'ProfileController@viewProfile');
Route::post('/viewProfileUser/{id}', 'ProfileController@viewProfileUser')->middleware('auth');
// user list
Route::get('/userlist', 'VerificationController@userList')->middleware('isAdmin');
// approve user verification
Route::patch('/approveUser/{id}', 'VerificationController@approveUser')->middleware('auth');

// confirm user verification
Route::patch('/approveUserConfirm/{id}', 'VerificationController@approveUserConfirm')->middleware('auth');
// booking a request
Route::get('/bookRequest', 'RequestController@bookRequest')->middleware('auth');
Route::post('/bookRequestUser/{id}', 'RequestController@bookRequestUser')->middleware('auth');
Route::post('/bookRequestForm/{id}', 'RequestController@bookRequestForm')->middleware('auth');
// change request pending to pendingConfirm
Route::patch('/pendingConfirm/{id}', 'RequestController@pendingConfirm')->middleware('auth');
//showing the booker profile
Route::patch('/viewProfileConfirm/{id}', 'RequestController@viewProfileConfirm')->middleware('auth');

Route::get('/dateList', 'RequestController@dateList')->middleware('auth');
Route::patch('/dateListConfirm/{id}', 'RequestController@dateListConfirm')->middleware('auth');

// paypal payment

Route::post('/paypal', 'PaymentController@createPayment')->name('create-payment');
Route::get('/confirm', 'PaymentController@confirmPayment')->name('confirm-payment');
