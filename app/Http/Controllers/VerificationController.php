<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Session;

class VerificationController extends Controller
{
    public function index(){
        return view('pages.verifyPage');
    }
    public function verifyAccount(Request $request, $id ){

        $validate = [
       
            "validId" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            "lastName" => "required",
            "Blk" => "required",
            "City" => "required",
            "Province" => "required",
            "Birthday"  => "required",
            "Age" => "required",
            "Gender" => "required",
            "contactNumber" => "required",
            "nationality" => "required",
            "country" => "required"
        ];

        $this->validate($request, $validate);
        
        if($request->hasfile('validId')){
            $file = $request->file('validId');
         
          
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('verification', $filename);

         
            $client = new Client;
            $response = $client->patch("localhost:3000/api/users/$id/verifyAccount", [
                "json" => [
                  
                    "firstName" => $request->firstName,
                    "lastName" => $request->lastName,
                    "Blk" => $request->Blk,
                    "City" => $request->City,
                    "Province" => $request->Province,
                    "Birthday" => $request->Birthday,
                    "Age" => $request->Age,
                    "Gender" => $request->Gender,
                    "Contact" => $request->contactNumber,
                    "Nationality" => $request->nationality,
                    "validId" => $filename,
                    "Country" => $request->country
                ]
    
    
            ]);

            Session::flash('verify', '"Your account verification is still pending for checking."');
                
                    
            return redirect('/myProfile');
    }
}

            public function userList(){

                
                $client = new Client;
                $response = $client->get("localhost:3000/api/users/");
        // json decode converts it on php array
                $usersall = collect(json_decode($response->getBody())->users)->reverse();
        
             
        
                return view('pages.userlist', compact('usersall'));

            }
            public function approveUser(Request $request, $id){
       

  $client = new Client;
            $response = $client->patch("localhost:3000/api/users/$id/approveUser", [
                "json" => [
                  
                  "approveUser" => $request->approveUser
                ]
    
    
            ]);

            Session::flash('approve', 'You approve a new user');
            Session::flash('approveNotif', "Congratulations your account is now verified");
            return redirect('/userlist');

            }
            public function approveUserConfirm(Request $request, $id){
       

                $client = new Client;
                          $response = $client->patch("localhost:3000/api/users/$id/approveUser", [
                              "json" => [
                                
                                "approveUser" => $request->approveUserConfirm
                              ]
                  
                  
                          ]);
              
                      
                          return redirect('/homepage');
              
                          }
                         
}