<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Session;

class ProfileController extends Controller
{
    public function index(){

        $users_id=Session::get('user')->_id;

       
       
        $client = new Client;
        $response = $client->get("localhost:3000/api/users/$users_id");
// json decode converts it on php array
        $user = json_decode($response->getBody())->users;
    
        $usersall= collect(json_decode($response->getBody())->usersall)->shuffle()->slice(0,3);
        
        return view('pages.myProfile',compact ('user', 'usersall'));
    }
    public function viewProfile(){
         $users_id= Session::get('dater')->_id;


       
       
        $client = new Client;
        $response = $client->get("localhost:3000/api/users/$users_id");
// json decode converts it on php array
        $user = json_decode($response->getBody())->users;
        $usersall= collect(json_decode($response->getBody())->usersall)->shuffle()->slice(0,3);
        
        return view('pages.viewProfile', compact('user', 'usersall'));
       
    }
    public function viewProfileUser(Request $request, $id){

        $users_id= $id;

        

  
       
       
        $client = new Client;
        $response = $client->get("localhost:3000/api/users/$users_id");
// json decode converts it on php array
        $user = json_decode($response->getBody())->users;
        
        Session::put('dater', $user);

        
        
        return redirect('/viewProfile');
       
    }

    public function profilePic(Request $request, $id)
    {
        
        if($request->hasfile('profilePic')){
            $file = $request->file('profilePic');
         
          
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads', $filename);

         
            $client = new Client;
            $response = $client->patch("localhost:3000/api/users/$id/profilePic", [
                "json" => [
                  
                    "profilePic" => $filename
    
                ]
    
    
            ]);
                
                    
            return redirect('/myProfile');
         
    }
    
}
public function editCaption(Request $request, $id){

    $validate = [
       
        "editCaption" => 'required',
       
    ];

    $this->validate($request, $validate);




    $client = new Client;
    $response = $client->patch("localhost:3000/api/users/$id/editCaption", [
        "json" => [
       
            "editCaption" => $request->editCaption,
          

        ]


    ]);
    Session::flash('editAbout', 'You successfully Edit Caption');

    return redirect('/myProfile');


        
}
    public function coverPic(Request $request, $id)
    {
        
        if($request->hasfile('coverPic')){
            $file = $request->file('coverPic');
         
          
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads', $filename);

         
            $client = new Client;
            $response = $client->patch("localhost:3000/api/users/$id/coverPic", [
                "json" => [
                  
                    "coverPic" => $filename
    
                ]
    
    
            ]);
                
                    
            return redirect('/myProfile');
         
    }
}

    public function editAbout(Request $request, $id)
    {

        $client = new Client;
        $response = $client->patch("localhost:3000/api/users/$id/editAbout", [
            "json" => [
           
                "Height" => $request->Height,
                "Weight" => $request->Weight,
                "Relationship" => $request->Relationship,
                "Smoke" => $request->Smoke,
                "Drink" => $request->Drink,
                "Work" => $request->Work,
               

            ]


        ]);
        Session::flash('editAbout', 'You successfully edit your abouts!');

        return redirect('/myProfile');

    }
    public function lookingFor(Request $request, $id)
    {

        $client = new Client;
        $response = $client->patch("localhost:3000/api/users/$id/lookingFor", [
            "json" => [
           
                "Gender" => $request->Gender,
                "Description" => $request->Description,
             

            ]


        ]);
        Session::flash('editAbout', "You successfully edit what your'e looking for!");
        return redirect('/myProfile');

    }
    public function myInterest(Request $request, $id)
    {

        $client = new Client;
        $response = $client->patch("localhost:3000/api/users/$id/myInterest", [
            "json" => [
           
                "Interest" => $request->myInterest,
          
             

            ]


        ]);
        Session::flash('editAbout', 'You successfully add a interest!');
        return redirect('/myProfile');

    }

    public function joinUs(Request $request, $id){

     
  
    
    
        $client = new Client;
        $response = $client->patch("localhost:3000/api/users/$id/joinUs", [
            "json" => [
           
                "pricePerHour" => $request->pricePerHour,

                "paypalEmail" => $request->paypalEmail,
                "userDate" => $request->userDate,
    
            ]
    
    
        ]);
        Session::flash('editAbout', 'Your Profile Is Been Posted On Homepage Goodluck!');
    
        return redirect('/homepage');
    
    
            
    }
    
}
