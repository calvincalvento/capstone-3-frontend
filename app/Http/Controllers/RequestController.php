<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Session;


class RequestController extends Controller
{
    public function bookRequest(){
        $users_id= Session::get('daterBooker')->_id;




        $client = new Client;
        $response = $client->get("localhost:3000/api/users/$users_id");
// json decode converts it on php array
        $user = json_decode($response->getBody())->users;
        $daterequests = json_decode($response->getBody())->requests;
          return view ('pages.bookRequest', compact('user', 'daterequests'));
      }
      public function bookRequestUser(Request $request, $id){
        $users_id= $id;






        $client = new Client;
        $response = $client->get("localhost:3000/api/users/$users_id");
// json decode converts it on php array
        $user = json_decode($response->getBody())->users;
        
        Session::put('daterBooker', $user);
        return redirect('/bookRequest');
      }
      public function bookRequestForm(Request $request, $id){
        $validate = [
       
            "date" => 'required',
            "timestart" => 'required',
            "timestop" => 'required',
            "location" => 'required',
           
        ];

        $this->validate($request, $validate);

      
        
        $client = new Client;
        $response = $client->post('localhost:3000/api/bookrequests', [
            "json" => [

                "bookerId" => $request->userId,
                "daterId" => $id,
                "date" => $request->date,
                "locationName" => $request->location,
                "latitude" => $request->lat,
                "longhitude" => $request->lng,
                "startTime" => $request->timestart,
                "endTime" => $request->timestop,
                "price" => $request->overallPrice



            ]


        ]);

       
        $daterequest = json_decode($response->getBody());

        
        Session::put('daterequest', $daterequest);

     

        Session::flash('daterequestSuccess', "You Successfully Request a Date!");
       
            
        return redirect('/homepage');
      }
      public function pendingConfirm(Request $request, $id){

      


        $client = new Client;
        $response = $client->patch("localhost:3000/api/bookrequests/$id/pendingConfirm", [
            "json" => [
           
                "pendingConfirm" => $request->pendingConfirm
            ]


        ]);
       

        return redirect('/homepage');

      }
      public function viewProfileConfirm(Request $request, $id){

      


        $client = new Client;
        $response = $client->patch("localhost:3000/api/bookrequests/$id/viewProfileConfirm", [
            "json" => [
           
                "pendingConfirm" => $request->pendingConfirm,
                "bookerId" => $request->bookerId
            ]


        ]);
       
        $user = json_decode($response->getBody())->users;
 
        $usersall= collect(json_decode($response->getBody())->userall)->shuffle()->slice(0,3);
        return view('pages.viewProfile' ,compact('user', 'usersall'));

      }
      // showing of datelist request
      public function dateList(){

        $client = new Client;
        $response = $client->get("localhost:3000/api/users/");
// json decode converts it on php array
        $usersall = collect(json_decode($response->getBody())->users)->reverse();
        $requests = collect(json_decode($response->getBody())->requests)->reverse();
        $users_id=Session::get('user')->_id;
        $client = new Client;
        $response1 = $client->get("localhost:3000/api/users/$users_id");
// json decode converts it on php array
        $user = json_decode($response1->getBody());
    
      $requestsall = $requests->filter(function($request){

        return  ($request->daterId == Session::get('user')->_id && $request->status == "pendingConfirm");
      }
      );

      $requestsall1 = $requests->filter(function($request){

        return  ($request->daterId == Session::get('user')->_id && $request->status != "pendingConfirm");
      }
      );
      $requestsall2 = $requests->filter(function($request){

        return  ($request->bookerId == Session::get('user')->_id && $request->status != "pendingConfirm");
      }
      );
    


        return view('pages.dateList', compact('usersall','user', 'requestsall', 'requestsall1', 'requestsall2'));

      }
      public function dateListConfirm(Request $request, $id){

      


        $client = new Client;
        $response = $client->patch("localhost:3000/api/bookrequests/$id/pendingConfirm", [
            "json" => [
           
                "pendingConfirm" => $request->pendingConfirm
            ]


        ]);
       

        return redirect('/dateList');

      }

}
