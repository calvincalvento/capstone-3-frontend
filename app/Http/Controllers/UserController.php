<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Session;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        
        
        $client = new Client;
        $response = $client->get("localhost:3000/api/users/");
// json decode converts it on php array
        $usersall = collect(json_decode($response->getBody())->users)->reverse();
        $requestsall = collect(json_decode($response->getBody())->requests)->reverse();
        $users_id=Session::get('user')->_id;
        $client = new Client;
        $response1 = $client->get("localhost:3000/api/users/$users_id");

        

// json decode converts it on php array
        $user = json_decode($response1->getBody())->users;
    
      

        return view('pages.homepage', compact('usersall','user', 'requestsall'));
    }
    public function landing(){
        $client = new Client;
        $response = $client->get("localhost:3000/api/users/");
// json decode converts it on php array
        $usersall = collect(json_decode($response->getBody())->users)->reverse();
        $requestsall = collect(json_decode($response->getBody())->requests)->reverse();



// json decode converts it on php arra
    
      

        return view('pages.landingpage', compact('usersall', 'requestsall'));

    }
    public function search(Request $request){

       

        $client = new Client;
        $response = $client->post('localhost:3000/api/users/search', [
            "json" => [

                "search" => $request->search
                
            ]


        ]);
// json decode converts it on php array
        $usersall = collect(json_decode($response->getBody())->users)->reverse();
        $requestsall = collect(json_decode($response->getBody())->requests)->reverse();
        $users_id=Session::get('user')->_id;
        $client = new Client;
        $response1 = $client->get("localhost:3000/api/users/$users_id");


// json decode converts it on php array
        $user = json_decode($response1->getBody())->users;
    
             

        return view('pages.search', compact('usersall','user', 'requestsall'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validate = [
       
            "firstName" => 'required',
            "lastName" => 'required',
            "email" => 'required',
          
            "password" => 'required'
        ];

        $this->validate($request, $validate);
        
        $client = new Client;
        $response = $client->post('localhost:3000/api/users', [
            "json" => [

                "firstName" => $request->firstName,
                "lastName" => $request->lastName,
                "email" => $request->email,
              
                "password" => $request->password,

            ]


        ]);

       
        $name = json_decode($response->getBody());

        Session::put('token', $response->getHeader('x-auth-token')[0]);
        Session::put('user', $name->login);

        
       
            
        return redirect('/homepage');
    }

    public function login(Request $request)
    {
        
      try{
    $client = new Client;
        $response = $client->post('localhost:3000/api/auth', [
            "json" => [

                "email" => $request->email,
                "password" => $request->password

            ]


        ]);

        $name = json_decode($response->getBody());

    

        Session::put('token', $response->getHeader('x-auth-token')[0]);
         Session::put('user', $name->login);
        
  
          
             
        return redirect('/homepage');
    } catch (RequestException $e) {

    if ($e->getResponse()->getStatusCode() == '400') {
         $response = $e->getResponse();
    $responseBodyAsString = json_decode($response->getBody());
            Session::flash('password',$responseBodyAsString->message);
            return redirect('/');
    }
}
    }
public function logout(Request $request){

    Session::flush();
    return redirect('/');


}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
